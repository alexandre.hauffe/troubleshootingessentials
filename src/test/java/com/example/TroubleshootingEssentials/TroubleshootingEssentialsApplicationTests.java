package com.example.TroubleshootingEssentials;

import com.example.TroubleshootingEssentials.model.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TroubleshootingEssentialsApplicationTests {

	@Autowired
	private TestRestTemplate testRestTemplate;

	private List<Book> books;

	@BeforeEach
	void setUp() {
		books = prepareList();
	}

	@Test
	void createUser() {
		//Act
		ResponseEntity responseEntity = testRestTemplate.postForEntity("/", books.get(0), Book.class);
		Book book = (Book) responseEntity.getBody();

		//Assert
		assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNotNull(book);
		assertEquals(books.get(0).getTitle(), book.getTitle());
	}

	@Test
	void getAllUsers() {
		//Arrange
		testRestTemplate.postForEntity("/", books.get(1), Book.class);
		testRestTemplate.postForEntity("/", books.get(2), Book.class);

		//Act
		ResponseEntity bookListResponse = testRestTemplate.getForEntity("/", Iterable.class);
		List<Book> booksRetrieved = (List<Book>) bookListResponse.getBody();

		//Assert
		assertNotNull(booksRetrieved);
		assertEquals(this.books.size(), booksRetrieved.size());
	}

	@Test
	void getOneUser() {
		//Arrange
		ResponseEntity bookCreatedResponse = testRestTemplate.postForEntity("/", books.get(2), Book.class);
		Book bookCreated = (Book) bookCreatedResponse.getBody();
		String id = "/"+bookCreated.getId();

		//Act
		ResponseEntity userRetrievedResponse = testRestTemplate.getForEntity(id, Book.class);
		Book userRetrieved = (Book) userRetrievedResponse.getBody();

		//Assert
		assertEquals(ResponseEntity.ok().build().getStatusCode(), userRetrievedResponse.getStatusCode());
		assertNotNull(userRetrievedResponse.getBody());
		assertNotNull(userRetrieved);
		assertEquals(bookCreated, userRetrieved);
	}

	@Test
	void DeleteUser() {
		//Arrange
		ResponseEntity bookCreatedResponse = testRestTemplate.postForEntity("/", books.get(0), Book.class);
		Book bookCreated = (Book) bookCreatedResponse.getBody();
		String id = "/"+bookCreated.getId();

		//Act
		testRestTemplate.delete(id);
		ResponseEntity bookRetrievedEntity = testRestTemplate.getForEntity(id, Book.class);
		Book bookRetrieved = (Book) bookRetrievedEntity.getBody();

		//Assert
		assertEquals(ResponseEntity.notFound().build().getStatusCode(), bookRetrievedEntity.getStatusCode());
		assertNull(bookRetrievedEntity.getBody());
		assertNull(bookRetrieved);
	}

	@Test
	void Update() {
		//Arrange
		ResponseEntity bookCreatedResponse = testRestTemplate.postForEntity("/", books.get(2), Book.class);
		Book bookCreated = (Book) bookCreatedResponse.getBody();
		String id = "/"+bookCreated.getId();
		bookCreated.setTitle("The book");

		//Act
		testRestTemplate.put(id, bookCreated);
		ResponseEntity bookRetrievedEntity = testRestTemplate.getForEntity(id, Book.class);
		Book bookUpdated = (Book) bookRetrievedEntity.getBody();

		//Assert
		assertEquals(ResponseEntity.ok().build().getStatusCode(), bookRetrievedEntity.getStatusCode());
		assertNotNull(bookRetrievedEntity.getBody());
		assertNotNull(bookUpdated);
		assertNotEquals(books.get(2), bookUpdated);
		assertNotEquals(books.get(2).getTitle(), bookUpdated.getTitle());
	}


	private List<Book> prepareList(){
		List<Book> list= new ArrayList<>();
		list.add(new Book("book1", "tittle1"));
		list.add(new Book("book2", "tittle2"));
		list.add(new Book("book3", "tittle3"));
		return list;
	}

}

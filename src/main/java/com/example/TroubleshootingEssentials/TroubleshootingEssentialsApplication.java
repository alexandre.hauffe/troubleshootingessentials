package com.example.TroubleshootingEssentials;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TroubleshootingEssentialsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TroubleshootingEssentialsApplication.class, args);
	}

}

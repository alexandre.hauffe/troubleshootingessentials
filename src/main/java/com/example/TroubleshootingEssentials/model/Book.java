package com.example.TroubleshootingEssentials.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString @EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "USER")
public class Book {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    @Getter @Setter
    private Long id;
    @Column(name = "TITLE")
    @Getter @Setter private String title;
    @Column(name = "AUTHOR")
    @Getter @Setter private String author;

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }
}

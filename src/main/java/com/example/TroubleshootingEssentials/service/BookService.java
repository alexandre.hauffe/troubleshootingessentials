package com.example.TroubleshootingEssentials.service;

import com.example.TroubleshootingEssentials.dao.BookRepository;
import com.example.TroubleshootingEssentials.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    private static final Logger logger = LoggerFactory.getLogger(BookService.class);

    @Autowired
    private final BookRepository repository;

    @Autowired
    public BookService(BookRepository repository){
        this.repository = repository;
    }

    public Iterable<Book> findAll(){
        logger.info("'findAll' called");

        return repository.findAll();
    }

    public ResponseEntity findById(Long id){
        ResponseEntity responseEntity = repository.findById(id)
                    .map(record -> ResponseEntity.ok().body(record))
                    .orElse(ResponseEntity.notFound().build());

        logger.info("'findById' method called with Id= {} ", id);

        if(ResponseEntity.notFound().build().equals(responseEntity))
            logger.warn("Can't findById with id {} from repository ", id);

        return responseEntity;
    }

    public Book create(Book book){
        Book bookCreate = book;
        String message = testObject(book);

        logger.info("'create' method called with book= {} ", book);

        if(!"".equals(message))
            logger.error("Can't save {} in repository because {}", book, message);
        else{
            bookCreate = repository.save(book);
        }

        return bookCreate;
    }

    public ResponseEntity <?> delete(Long id){
        ResponseEntity responseEntity = repository.findById(id)
                    .map(record -> {
                        repository.deleteById(id);
                        return ResponseEntity.ok().build();
                    }).orElse(ResponseEntity.notFound().build());

        logger.info("'delete' method called with book id= {} ", id);

        if(ResponseEntity.notFound().build().equals(responseEntity))
            logger.warn("Can't findById with id {} from repository ", id);

        return responseEntity;
    }


    public ResponseEntity update(long id, Book book) {
        ResponseEntity responseEntity = repository.findById(id)
                    .map(record -> {
                        record.setTitle(book.getTitle());
                        record.setAuthor(book.getAuthor());
                        Book bookUpdated = repository.save(record);
                        return ResponseEntity.ok().body(bookUpdated);
                    }).orElse(ResponseEntity.notFound().build());

        logger.info("'update' method called with book= {} and id= {}", book, id);

        if(ResponseEntity.notFound().build().equals(responseEntity))
            logger.error("Can't update book id={} to book={} in repository", id, book);

        return responseEntity;
    }

    private String testObject(Book book){
        String message = "";
        if(book.getTitle() == null){
            message += "Title is null ";
        }
        if(book.getAuthor() == null){
            message += "Author is null ";
        }
        return message;
    }

}

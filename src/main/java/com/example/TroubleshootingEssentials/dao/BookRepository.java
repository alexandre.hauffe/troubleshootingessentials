package com.example.TroubleshootingEssentials.dao;

import com.example.TroubleshootingEssentials.model.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {

}

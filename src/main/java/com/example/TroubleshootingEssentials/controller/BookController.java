package com.example.TroubleshootingEssentials.controller;


import com.example.TroubleshootingEssentials.model.Book;
import com.example.TroubleshootingEssentials.service.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/")
public class BookController {

    private BookService service;

    public BookController(BookService service) {
        this.service = service;
    }

    @RequestMapping("server")
    public String hello() {
        return "Service for Troubleshooting Essentials";
    }

    @GetMapping
    public Iterable<Book> findAll(){
        return service.findAll();
    }

    @GetMapping(path = {"{id}"})
    public ResponseEntity<?> findById(@PathVariable long id){
        return service.findById(id);
    }

    @PostMapping
    public Book create(@RequestBody Book user){
        return service.create(user);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity update(@PathVariable("id") long id, @RequestBody Book user) {
        return service.update(id, user);
    }

    @DeleteMapping(path ={"{id}"})
    public ResponseEntity <?> delete(@PathVariable long id) {
        return service.delete(id);
    }

}
